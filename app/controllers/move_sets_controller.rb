class MoveSetsController < ApplicationController
  before_action :set_move_set, only: [:show, :edit, :update, :destroy]

  # GET /move_sets
  # GET /move_sets.json
  def index
    @move_sets = MoveSet.all
  end

  # GET /move_sets/1
  # GET /move_sets/1.json
  def show
  end

  # GET /move_sets/new
  def new
    @move_set = MoveSet.new
  end

  # GET /move_sets/1/edit
  def edit
  end

  # POST /move_sets
  # POST /move_sets.json
  def create
    @move_set = MoveSet.new(move_set_params)

    respond_to do |format|
      if @move_set.save
        format.html { redirect_to @move_set, notice: 'Move set was successfully created.' }
        format.json { render :show, status: :created, location: @move_set }
      else
        format.html { render :new }
        format.json { render json: @move_set.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /move_sets/1
  # PATCH/PUT /move_sets/1.json
  def update
    respond_to do |format|
      if @move_set.update(move_set_params)
        format.html { redirect_to @move_set, notice: 'Move set was successfully updated.' }
        format.json { render :show, status: :ok, location: @move_set }
      else
        format.html { render :edit }
        format.json { render json: @move_set.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /move_sets/1
  # DELETE /move_sets/1.json
  def destroy
    @move_set.destroy
    respond_to do |format|
      format.html { redirect_to move_sets_url, notice: 'Move set was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_move_set
      @move_set = MoveSet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def move_set_params
      params.require(:move_set).permit(:name, :requirements, :power, :effects, :mana_effects, :image, :sfx, :vfx)
    end
end
