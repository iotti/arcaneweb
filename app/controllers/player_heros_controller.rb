class PlayerHerosController < ApplicationController
  before_action :set_player_hero, only: [:show, :edit, :update, :destroy]

  # GET /player_heros
  # GET /player_heros.json
  def index
    @player_heros = PlayerHero.all
  end

  # GET /player_heros/1
  # GET /player_heros/1.json
  def show
  end

  # GET /player_heros/new
  def new
    @player_hero = PlayerHero.new
  end

  # GET /player_heros/1/edit
  def edit
  end

  # POST /player_heros
  # POST /player_heros.json
  def create
    @player_hero = PlayerHero.new(player_hero_params)

    respond_to do |format|
      if @player_hero.save
        format.html { redirect_to @player_hero, notice: 'Player hero was successfully created.' }
        format.json { render :show, status: :created, location: @player_hero }
      else
        format.html { render :new }
        format.json { render json: @player_hero.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /player_heros/1
  # PATCH/PUT /player_heros/1.json
  def update
    respond_to do |format|
      if @player_hero.update(player_hero_params)
        format.html { redirect_to @player_hero, notice: 'Player hero was successfully updated.' }
        format.json { render :show, status: :ok, location: @player_hero }
      else
        format.html { render :edit }
        format.json { render json: @player_hero.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /player_heros/1
  # DELETE /player_heros/1.json
  def destroy
    @player_hero.destroy
    respond_to do |format|
      format.html { redirect_to player_heros_url, notice: 'Player hero was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_player_hero
      @player_hero = PlayerHero.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def player_hero_params
      params.require(:player_hero).permit(:player_id, :bat_fire, :bat_earth, :bat_water, :bat_wind, :atk, :life, :def)
    end
end
