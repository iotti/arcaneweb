class Battle< ApplicationRecord
  has_many :battle_logs
  has_one :player_challenger, foreign_key: :player_challenger, class: Player
  has_one :player_challenged, foreign_key: :player_challenged, class: Player
  has_one :hero_challenger, foreign_key: :hero_challenger, class: PlayerHero
  has_one :hero_challenged, foreign_key: :hero_challenged, class: PlayerHero

  after_create :battle_begin

  DEFAULT_TURN_TIME = 60

  private
  def battle_begin
    started_at = DateTime.now
    max_turn_time = DEFAULT_TURN_TIME
    hero_challenger.reset_stats()
    hero_challenged.reset_stats()
  end
end
