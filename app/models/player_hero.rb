class PlayerHero < ApplicationRecord
  belongs_to :player
  belongs_to :hero

  def reset_stats
    self.atk = hero.max_atk
    self.def = hero.max_def
    self.life = hero.max_life
  end
end
