json.extract! hero, :id, :name, :max_fire, :max_earth, :max_water, :max_wind, :max_atk, :max_lif, :max_def, :portarit_pic, :front_image, :back_image, :created_at, :updated_at
json.url hero_url(hero, format: :json)
