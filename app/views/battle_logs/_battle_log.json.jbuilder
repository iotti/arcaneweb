json.extract! battle_log, :id, :battle_id, :action, :player_hero_atk, :player_hero_def, :result, :created_at, :updated_at
json.url battle_log_url(battle_log, format: :json)
