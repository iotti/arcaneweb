json.extract! player_hero, :id, :player_id, :bat_fire, :bat_earth, :bat_water, :bat_wind, :atk, :life, :def, :created_at, :updated_at
json.url player_hero_url(player_hero, format: :json)
