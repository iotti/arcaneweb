json.extract! move_set, :id, :name, :requirements, :power, :effects, :mana_effects, :image, :sfx, :vfx, :created_at, :updated_at
json.url move_set_url(move_set, format: :json)
