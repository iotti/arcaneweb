Rails.application.routes.draw do
  resources :move_sets
  resources :battle_logs
  resources :player_heros
  resources :heros
  devise_for :players
  devise_scope :player do
    root to: "devise/sessions#new"
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
