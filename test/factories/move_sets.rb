FactoryGirl.define do
  factory :move_set do
    name ""
    requirements ""
    power ""
    effects ""
    mana_effects ""
    image ""
    sfx ""
    vfx "MyString"
  end
end
