require 'test_helper'

class PlayerHerosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @player_hero = player_heros(:one)
  end

  test "should get index" do
    get player_heros_url
    assert_response :success
  end

  test "should get new" do
    get new_player_hero_url
    assert_response :success
  end

  test "should create player_hero" do
    assert_difference('PlayerHero.count') do
      post player_heros_url, params: { player_hero: { atk: @player_hero.atk, bat_earth: @player_hero.bat_earth, bat_fire: @player_hero.bat_fire, bat_water: @player_hero.bat_water, bat_wind: @player_hero.bat_wind, def: @player_hero.def, life: @player_hero.life, player_id: @player_hero.player_id } }
    end

    assert_redirected_to player_hero_url(PlayerHero.last)
  end

  test "should show player_hero" do
    get player_hero_url(@player_hero)
    assert_response :success
  end

  test "should get edit" do
    get edit_player_hero_url(@player_hero)
    assert_response :success
  end

  test "should update player_hero" do
    patch player_hero_url(@player_hero), params: { player_hero: { atk: @player_hero.atk, bat_earth: @player_hero.bat_earth, bat_fire: @player_hero.bat_fire, bat_water: @player_hero.bat_water, bat_wind: @player_hero.bat_wind, def: @player_hero.def, life: @player_hero.life, player_id: @player_hero.player_id } }
    assert_redirected_to player_hero_url(@player_hero)
  end

  test "should destroy player_hero" do
    assert_difference('PlayerHero.count', -1) do
      delete player_hero_url(@player_hero)
    end

    assert_redirected_to player_heros_url
  end
end
