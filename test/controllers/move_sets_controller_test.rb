require 'test_helper'

class MoveSetsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @move_set = move_sets(:one)
  end

  test "should get index" do
    get move_sets_url
    assert_response :success
  end

  test "should get new" do
    get new_move_set_url
    assert_response :success
  end

  test "should create move_set" do
    assert_difference('MoveSet.count') do
      post move_sets_url, params: { move_set: { effects: @move_set.effects, image: @move_set.image, mana_effects: @move_set.mana_effects, name: @move_set.name, power: @move_set.power, requirements: @move_set.requirements, sfx: @move_set.sfx, vfx: @move_set.vfx } }
    end

    assert_redirected_to move_set_url(MoveSet.last)
  end

  test "should show move_set" do
    get move_set_url(@move_set)
    assert_response :success
  end

  test "should get edit" do
    get edit_move_set_url(@move_set)
    assert_response :success
  end

  test "should update move_set" do
    patch move_set_url(@move_set), params: { move_set: { effects: @move_set.effects, image: @move_set.image, mana_effects: @move_set.mana_effects, name: @move_set.name, power: @move_set.power, requirements: @move_set.requirements, sfx: @move_set.sfx, vfx: @move_set.vfx } }
    assert_redirected_to move_set_url(@move_set)
  end

  test "should destroy move_set" do
    assert_difference('MoveSet.count', -1) do
      delete move_set_url(@move_set)
    end

    assert_redirected_to move_sets_url
  end
end
