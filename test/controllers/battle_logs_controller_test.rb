require 'test_helper'

class BattleLogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @battle_log = battle_logs(:one)
  end

  test "should get index" do
    get battle_logs_url
    assert_response :success
  end

  test "should get new" do
    get new_battle_log_url
    assert_response :success
  end

  test "should create battle_log" do
    assert_difference('BattleLog.count') do
      post battle_logs_url, params: { battle_log: { action: @battle_log.action, battle_id: @battle_log.battle_id, player_hero_atk: @battle_log.player_hero_atk, player_hero_def: @battle_log.player_hero_def, result: @battle_log.result } }
    end

    assert_redirected_to battle_log_url(BattleLog.last)
  end

  test "should show battle_log" do
    get battle_log_url(@battle_log)
    assert_response :success
  end

  test "should get edit" do
    get edit_battle_log_url(@battle_log)
    assert_response :success
  end

  test "should update battle_log" do
    patch battle_log_url(@battle_log), params: { battle_log: { action: @battle_log.action, battle_id: @battle_log.battle_id, player_hero_atk: @battle_log.player_hero_atk, player_hero_def: @battle_log.player_hero_def, result: @battle_log.result } }
    assert_redirected_to battle_log_url(@battle_log)
  end

  test "should destroy battle_log" do
    assert_difference('BattleLog.count', -1) do
      delete battle_log_url(@battle_log)
    end

    assert_redirected_to battle_logs_url
  end
end
