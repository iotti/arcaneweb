class CreateHeros < ActiveRecord::Migration[5.1]
  def change
    create_table :heros do |t|
      t.string :name
      t.string :hero_class
      t.integer :max_fire
      t.integer :max_earth
      t.integer :max_water
      t.integer :max_wind
      t.integer :max_atk
      t.integer :max_lif
      t.integer :max_def
      t.string :portarit_pic
      t.string :front_image
      t.string :back_image

      t.timestamps
    end

    Hero.create name: 'Watton', hero_class: 'Knight', max_fire: 3, max_earth: 4,
      max_water: 1, max_wind: 2, max_atk:4, max_def: 6, 
      max_lif: 12, portarit_pic: '', front_image: '', back_image:''

    Hero.create name: 'Lilian', hero_class: 'Sorceress', max_fire: 6, max_earth: 3,
      max_water: 3, max_wind: 4, max_atk:2, max_def: 3, 
      max_lif: 8, portarit_pic: '', front_image: '', back_image:''

    Hero.create name: 'Hexel', hero_class: 'Druid', max_fire: 1, max_earth: 7,
      max_water: 5, max_wind: 4, max_atk:3, max_def: 2, 
      max_lif: 10, portarit_pic: '', front_image: '', back_image:''
    
  end
end
