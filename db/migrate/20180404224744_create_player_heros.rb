class CreatePlayerHeros < ActiveRecord::Migration[5.1]
  def change
    create_table :player_heros, id: false do |t|
      t.integer :id, limit: 8, null: false, unsigned: true
      t.references :player, foreign_key: true, null: false
      t.references :hero, foreign_key: true, null: false
      t.integer :bat_fire
      t.integer :bat_earth
      t.integer :bat_water
      t.integer :bat_wind
      t.integer :atk
      t.integer :life
      t.integer :def

      t.references :battle, foreign_key: true

      t.timestamps
    end
  end
end
