class Battle < ActiveRecord::Migration[5.1]
  def change
    create_table :battles, id: false do |t|

      t.integer :id, limit: 8, null: false, unsigned: true

      t.integer :player_challanger_id, limit: 8, null: false, unsigned: true
      t.integer :player_challanged_id, limit: 8, null: false, unsigned: true

      t.integer :hero_challanger_id, limit: 8, null: false, unsigned: true
      t.integer :hero_challanged_id, limit: 8, null: false, unsigned: true

      t.timestamp :started_at
      t.timestamp :finished_at

      t.boolean :challanger_winner

      t.boolean :challanger_turn
      t.timestamp :last_move

      t.integer :max_turn_time, unsigned: true

      t.timestamps null: false
    end

    add_index :battles, :id, unique: true
    add_index :battles, :player_challanger_id
    add_index :battles, :player_challanged_id

    add_foreign_key :battles, :players, column: :player_challanger_id
    add_foreign_key :battles, :players, column: :player_challanged_id

    add_foreign_key :battles, :player_heros, column: :hero_challanger_id
    add_foreign_key :battles, :player_heros, column: :hero_challanged_id
  end
end
