class CreateMoveSets < ActiveRecord::Migration[5.1]
  def change
    create_table :move_sets do |t|
      t.string :name
      t.string :requirements, default: ''
      t.integer :power
      t.string :effects
      t.string :mana_effects
      t.string :image
      t.string :sfx
      t.string :vfx

      t.timestamps
    end

    MoveSet.create({name:'Base Attack', power:1})
  end
end
