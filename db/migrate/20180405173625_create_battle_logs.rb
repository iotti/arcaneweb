class CreateBattleLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :battle_logs do |t|
      t.references :battle, foreign_key: true
      t.string :actions
      t.integer :hero_atk_id, limit: 8, null: false, unsigned: true
      t.integer :hero_def_id, limit: 8, null: false, unsigned: true
      t.string :result

      t.timestamps
    end

    add_foreign_key :battle_logs, :player_heros, column: :hero_atk_id
    add_foreign_key :battle_logs, :player_heros, column: :hero_def_id
  end
end
