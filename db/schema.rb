# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180412044102) do

  create_table "battle_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "battle_id"
    t.string "actions"
    t.bigint "hero_atk_id", null: false
    t.bigint "hero_def_id", null: false
    t.string "result"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["battle_id"], name: "index_battle_logs_on_battle_id"
  end

  create_table "battles", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "id", null: false
    t.bigint "player_challanger_id", null: false
    t.bigint "player_challanged_id", null: false
    t.bigint "hero_challanger_id", null: false
    t.bigint "hero_challanged_id", null: false
    t.datetime "started_at"
    t.datetime "finished_at"
    t.boolean "challanger_winner"
    t.boolean "challanger_turn"
    t.datetime "last_move"
    t.integer "max_turn_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id"], name: "index_battles_on_id", unique: true
    t.index ["player_challanged_id"], name: "index_battles_on_player_challanged_id"
    t.index ["player_challanger_id"], name: "index_battles_on_player_challanger_id"
  end

  create_table "heros", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.string "hero_class"
    t.integer "max_fire"
    t.integer "max_earth"
    t.integer "max_water"
    t.integer "max_wind"
    t.integer "max_atk"
    t.integer "max_lif"
    t.integer "max_def"
    t.string "portarit_pic"
    t.string "front_image"
    t.string "back_image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "move_sets", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.string "requirements", default: ""
    t.integer "power"
    t.string "effects"
    t.string "mana_effects"
    t.string "image"
    t.string "sfx"
    t.string "vfx"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "player_heros", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "id", null: false
    t.integer "player_id", null: false
    t.integer "hero_id", null: false
    t.integer "bat_fire"
    t.integer "bat_earth"
    t.integer "bat_water"
    t.integer "bat_wind"
    t.integer "atk"
    t.integer "life"
    t.integer "def"
    t.integer "battle_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["battle_id"], name: "index_player_heros_on_battle_id"
    t.index ["hero_id"], name: "index_player_heros_on_hero_id"
    t.index ["player_id"], name: "index_player_heros_on_player_id"
  end

  create_table "players", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "id", null: false
    t.string "name", limit: 64, null: false
    t.bigint "battles", default: 0, null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.boolean "is_admin", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_players_on_email", unique: true
    t.index ["reset_password_token"], name: "index_players_on_reset_password_token", unique: true
  end

end
